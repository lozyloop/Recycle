package com.example.lozyloop.recycleview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initImageBitmaps();

    }

    private void initImageBitmaps(){
        Log.d(TAG,"initImageBitmaps : preparing bitmaps");

        mImageUrls.add("http://image.phimmoi.net/film/6436/preview.medium.jpg?_v=1530891178");
        mNames.add("Overlord");

        mImageUrls.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQoiE6JxvH2XKQ2lVOuM4Vvoi1j4vv0d9zkwjem5sNwVQy9pmQ-");
        mNames.add("SWORT ART ONLINE");

        mImageUrls.add("https://vignette.wikia.nocookie.net/log-horizon/images/9/93/Character_Slider.jpg/revision/latest/scale-to-width-down/670?cb=20180522155243");
        mNames.add("Log Horizon");

        mImageUrls.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScCbdJjZ2tlMFVDNS7B1cw0eiSm6YBhyorBupxTp34JHVzxnhN");
        mNames.add("Code geass");

        mImageUrls.add("https://static.next-episode.net/tv-shows-images/huge/death-march-kara-hajimaru-isekai-kyousoukyoku.jpg");
        mNames.add("Death march kara hajimaru ishekai");

        mImageUrls.add("http://image.phimmoi.net/film/5188/preview.medium.jpg?_v=1492111084");
        mNames.add("Mahouka");

        initRecyclerView();
    }

    private void initRecyclerView(){

        RecyclerView recyclerView = findViewById(R.id.myRecycle);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, mNames, mImageUrls);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }


}
